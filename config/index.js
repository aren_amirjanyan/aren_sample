var config = {
    production: {
        mode: 'production',
        port: 4000,
        mongodb_url :'',
        mongodb_name :'',
        jwt_secret:"",
        session_life_time: 600
    },
    local: {
        mode: 'local',
        port: 3000,
        mongodb_url :'mongodb://localhost:27017',
        mongodb_name :'aren_sample',
        jwt_secret:"AwOWQ4NGQwMTE0MjNkYzlmYjYiLCJuYW1lIjoib3JtZW4",
        session_life_time: 600,
    },
    staging: {
        mode: 'staging',
        port: 4000,
        mongodb_url :'',
        mongodb_name :'',
        jwt_secret:"",
        session_life_time: 600
    }
};
module.exports = function(mode) {
    return config[mode || process.argv[2] || 'local'] || config.local;
};