const app = require('express')(),
      config = require('./config')(),
      bodyParser = require('body-parser'),
      routes = require('./routes');

app.set('port',process.env.PORT || 3001);
app.use(bodyParser.json());

require('./middleware')(app);

require('./middleware/error')(app);

app.use('/api/v1',routes);

app.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

