'use strict';
const rp = require('request-promise');
class SymbolService {
    get_symbols(){
       return new Promise((resolve, reject) => {
           const options = {
               uri: 'https://api.iextrading.com/1.0/ref-data/symbols',
               headers: {
                   'User-Agent': 'Request-Promise'
               },
               json: true // Automatically parses the JSON string in the response
           };
           rp(options).then(response => {
               resolve(response);
           }).catch(error=> {
               reject(error);
           });
       });
    }
}

module.exports = new SymbolService();