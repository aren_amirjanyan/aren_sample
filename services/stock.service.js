'use strict';
const rp = require('request-promise'),
      cheerio = require('cheerio');
class StockService {

    async get_stock(symbol){
            const company       = await this.get_company(symbol).then(company=>company).catch(error=>error);
            const company_logo  = await this.get_company_logo(symbol).then(logo=>logo).catch(error=>error);
            const news          = await this.get_news(symbol).then(news=>news).catch(error=>error);
            const yahoo_data    = await this.get_yahoo_data(symbol).then(data=>data).catch(error=>error);
            return { company, company_logo, news, industries : {
                    industry_iex    : company.industry,
                    yahoo_data
                } };
    }
    get_company(symbol){
        const options = {
            uri: `https://api.iextrading.com/1.0/stock/${symbol}/company`,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        };
        return rp(options).then(company => {
            return company;
        }).catch(error => {
            return error;
        });

    }
    get_company_logo(symbol){
        const options = {
            uri: `https://api.iextrading.com/1.0/stock/${symbol}/logo`,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        };
        return rp(options).then(logo => {
            return logo;
        }).catch(error => {
            return error;
        });
    }
    get_news(symbol){
        const options = {
            uri: `https://api.iextrading.com/1.0/stock/${symbol}/news`,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        };
        return rp(options).then(news => {
            return news;
        }).catch(error => {
            return error;
        });
    }
    get_yahoo_data(symbol){
        const options = {
            uri: `https://finance.yahoo.com/quote/${symbol}?p=${symbol}`,
            headers: {
                'User-Agent': 'Request-Promise'
            },
            transform: function (body) {
                return cheerio.load(body);
            }
        };
        return rp(options)
            .then(function ($) {
                const sector = $('span').filter(function(value) {
                    return $(value).text().indexOf('Sector:') > -1;
                }).next().text();
                const industry = $('span').filter(function(value) {
                    return $(value).text().indexOf('Industry:') > -1;
                }).next().text();
                return {sector,industry};
            })
            .catch(function (error) {
                return error;
            });
    }
}

module.exports = new StockService();