import {Route, Switch, Redirect, withRouter} from "react-router-dom";
import Home from "../pages/Home";
import Stock from "../pages/Stock";
import NotFoundPage from "../pages/404";
import React, {Component} from "react";
import {connect} from 'react-redux';

class Router extends Component {
    render() {
        return (
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/stock/:symbol" exact component={Stock}/>
                <Route path="/404" exact component={NotFoundPage}/>
                <Redirect to='/404'/>
            </Switch>
        )
    }
}

export default withRouter(connect(
    state => (
        {
        }),
)(Router));