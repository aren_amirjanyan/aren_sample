import React, { Component } from "react";
import "../../assets/styles/App.css";
import '../../assets/styles/material-dashboard.css'
import {Router, Switch, Route} from "react-router-dom";
import history from './../../core/history'
import {connect} from "react-redux";
import MainContainer from '../../components/layouts/MainContainer'


class App extends Component {

    render() {
        return (
            <Router history = {history}>
                <Switch>
                    <Route  path="/" component={MainContainer}/>
                </Switch>
            </Router>
        );
    }


}

export default connect(
    state => (
        {
        }),
)(App);
