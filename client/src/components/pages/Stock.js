import React from 'react';
import {connect} from 'react-redux';
import { compose } from "redux";
import "../../assets/styles/Signin.css";
import {getStock} from '../../actions/StockAction';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import Company from '../partials/Company';
import CompanyLogo from '../partials/CompanyLogo';
import News from '../partials/News';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    card: {
        maxWidth: 400,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: -8,
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

class Stock extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            stock : {},
            expanded: false
        }
    }
    componentDidMount(){
        this.props.getStock(this.props.match.params.symbol);
    }
    componentDidUpdate(prevProps){
        if(prevProps.stock !== this.props.stock){
            this.setState({
                stock : this.props.stock
            })
        }
    }
    render() {
        const { classes, gettingStock } = this.props;
        return (
            <div className='Stock'>
                <div className="container">
                    <div className="row">
                        <div className="col-md-2">
                            {this.state.stock.company_logo && <CompanyLogo companyLogo = {this.state.stock.company_logo}/>}
                        </div>
                        <div className="col-md-10">
                            {this.state.stock.company && <Company company = {this.state.stock.company}/>}
                        </div>
                        {this.state.stock.news && <News newsItems = {this.state.stock.news} classes={classes}/>}
                    </div>
                </div>
                <div className="row">
                    { gettingStock && <CircularProgress disableShrink className={classes.progress} color="secondary" /> }
                </div>
            </div>
        )
    }
}

Stock.propTypes = {
    classes: PropTypes.object.isRequired
};
export default compose(
    withStyles(styles, {
        name: 'Stock',
    }),
    connect(
        state => (
            {
                stock           : state.stockReducer.stock,
                gettingStock    : state.stockReducer.gettingStock,
                getStockError   : state.stockReducer.getStockError,
                getStockSuccess : state.stockReducer.getStockSuccess
            }),
        {getStock}
    ),
)(Stock);
