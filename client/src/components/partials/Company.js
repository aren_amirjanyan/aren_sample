import React from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classNames from 'classnames';

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        marginTop : 50,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    }
});

class Company extends React.Component {
    state = {
        expanded: null,
    };

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };
    render() {
        const { company, classes } = this.props;
        const { expanded } = this.state;

        const expansionPanel = [];
        Object.keys(company).forEach((key, index)=>{
            if(company[key]){
                expansionPanel.push( <ExpansionPanel key={index} expanded={expanded === 'panel' + index} onChange={this.handleChange('panel'+index)}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>{key}</Typography>
                        <Typography className={classes.secondaryHeading}>{company[key]}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            {company[key]}
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>);
            }
        });

        return (
            <div className={classNames(classes.layout, classes.cardGrid)}>
                {expansionPanel}
            </div>
        )
    }
}

Company.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Company);
