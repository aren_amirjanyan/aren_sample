import React from "react";
import PropTypes from 'prop-types';
import '../../assets/styles/NavBar.css'
import {withStyles} from "@material-ui/core/styles/index";
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import { Link } from 'react-router-dom'

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    icon: {
        marginRight: theme.spacing.unit * 2,
    }
});
class NavBar extends React.Component {

    render() {
        const { classes } = this.props;
        return (
            <AppBar position="static" className={classes.appBar}>
                <Link to="/">
                    <Toolbar>
                    <CameraIcon className={classes.icon} />
                    <Typography variant="h6" noWrap>
                        Sample Project
                    </Typography>
                </Toolbar>
                </Link>
            </AppBar>
        )
    }
}

NavBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);
