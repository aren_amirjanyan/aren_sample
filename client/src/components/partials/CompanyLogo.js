import React from "react";
import PropTypes from 'prop-types';
import {withStyles} from "@material-ui/core/styles/index";
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        marginTop : 50,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardMedia: {
        height : "auto",
        width:100,
        paddingTop:30
    },
});

class CompanyLogo extends React.Component {
    render() {
        const { companyLogo, classes } = this.props;
        return (
            <div className={classNames(classes.layout, classes.cardGrid)}>
                <Typography className={classes.heading}>Logo</Typography>
                <img alt="Company Logo" src={companyLogo.url}/>
            </div>
        )
    }
}

CompanyLogo.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CompanyLogo);
