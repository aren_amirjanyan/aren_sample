import React, {Component} from "react";
import Routes from '../system/Router';
import {connect} from "react-redux";
import CssBaseline from '@material-ui/core/CssBaseline';
import NavBar from '../partials/NavBar';
import Footer from '../partials/Footer';

class MainContainer extends Component {

    render() {
            return (
                <React.Fragment>
                    <CssBaseline/>
                    <NavBar/>
                        <Routes/>
                    <Footer/>
                </React.Fragment>
            )
    }
}

export default connect(
    state => (
        {
        }),
)(MainContainer);

