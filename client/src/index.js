import React from 'react';
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from './core/store';
import App from "./components/system/App";
import registerServiceWorker from "./core/registerServiceWorker";
import "./assets/styles/index.css";

// import 'font-awesome/css/font-awesome.min.css';

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
registerServiceWorker();

