import {stockApiRoutes} from "../configs/routes"
import {
    GET_STOCK_REQUEST,
    GET_STOCK_SUCCESS,
    GET_STOCK_FAILURE
} from './types';

export const getStock = (symbol) => dispatch => {
    dispatch({type: GET_STOCK_REQUEST, gettingStock: true});
    fetch(stockApiRoutes.GET_STOCK + `/${symbol}`)
        .then(response => response.json())
        .then(stock => dispatch({type: GET_STOCK_SUCCESS, getStockSuccess:true, stock: stock}))
        .catch(error => dispatch({type: GET_STOCK_FAILURE, gettingStockError: true}))
};