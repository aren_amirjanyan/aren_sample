import {symbolsApiRoutes } from "../configs/routes"
import {GET_SYMBOLS_REQUEST, GET_SYMBOLS_SUCCESS, GET_SYMBOLS_FAILURE} from './types';

export const getSymbols = () => dispatch => {
    dispatch({type: GET_SYMBOLS_REQUEST, gettingSymbols: true});
    //fetch('https://api.iextrading.com/1.0/ref-data/symbols')
    fetch(symbolsApiRoutes.GET_SYMBOLS)
        .then(response => response.json())
        .then(symbols => dispatch({type: GET_SYMBOLS_SUCCESS, getSymbolsSuccess:true, symbols: symbols}))
        .catch(error => dispatch({type: GET_SYMBOLS_FAILURE, gettingSymbolsError: true}))
};
