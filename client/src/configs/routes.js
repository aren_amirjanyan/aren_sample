export const API_HOST = 'http://localhost:3001/api/v1';
/**
 * API Symbols routes
 */
export const symbolsApiRoutes = {
    GET_SYMBOLS : API_HOST + '/symbols',
};

/**
 * API Stock routes
 */
export const stockApiRoutes = {
    GET_STOCK : API_HOST + '/stock',
};
