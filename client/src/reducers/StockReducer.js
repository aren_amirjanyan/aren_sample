import {
    GET_STOCK_FAILURE,
    GET_STOCK_REQUEST,
    GET_STOCK_SUCCESS
} from '../actions/types';

const initialState = {
    gettingStock      :   false,
    getStockError     :   null,
    getStockSuccess   :   null,
    stock             : [],
};

export function stockReducer(state = initialState, action){
    switch (action.type) {
        case GET_STOCK_REQUEST : return {
        ...state,
            gettingStock : action.gettingStock
    };
        case GET_STOCK_SUCCESS : return {
            ...state,
            stock               : action.stock,
            getStockSuccess     : true,
            gettingStock        : false
        };
        case GET_STOCK_FAILURE : return {
            ...state,
            getStockError         : action.getStockError,
            getStockSuccess       : false,
            gettingStock          : false
        };
        default : return state;
    }
}