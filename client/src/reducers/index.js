import {combineReducers} from 'redux';
import {symbolsReducer} from "./SymbolReducer";
import {stockReducer} from "./StockReducer";

const rootReducer = combineReducers({
    symbolsReducer,
    stockReducer
});

export default rootReducer;