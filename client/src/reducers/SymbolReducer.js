import {GET_SYMBOLS_REQUEST, GET_SYMBOLS_SUCCESS, GET_SYMBOLS_FAILURE} from '../actions/types';

const initialState = {
    gettingSymbols      :   false,
    getSymbolsError     :   null,
    getSymbolsSuccess   :   null,
    symbols             : []
};

export function symbolsReducer(state = initialState, action){
    switch (action.type) {
        case GET_SYMBOLS_REQUEST : return {
            ...state,
            gettingSymbols : action.gettingSymbols
        };
        case GET_SYMBOLS_SUCCESS : return {
            ...state,
            symbols             : action.symbols,
            getSymbolsSuccess   : true,
            gettingSymbols      : false,
        };
        case GET_SYMBOLS_FAILURE : return {
            ...state,
            getSymbolsError         : action.getSymbolsError,
            getSymbolsSuccess       : false,
            gettingSymbols          : false,
        };
        default : return state;
    }
}