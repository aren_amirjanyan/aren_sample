const mongoose = require('../models/connection'),
    ObjectId = mongoose.Schema.ObjectId;

const symbolSchema = new mongoose.Schema({
    id: String
});

module.exports = mongoose.model('Symbol', symbolSchema);