const mongoose = require('../models/connection'),
    ObjectId = mongoose.Schema.ObjectId;

const stockSchema = new mongoose.Schema({
    id: String
});

module.exports = mongoose.model('Stock', stockSchema);