const express = require('express'),
    symbolController = require('./controllers/SymbolController'),
    stockController = require('./controllers/StockController');

const router = express.Router();

/**
 * Get Symbols
 */
router.get('/symbols',symbolController.get_symbols);

/**
 * Get Stocks
 */
router.get('/stock/:symbol',stockController.get_stock);

module.exports = router;