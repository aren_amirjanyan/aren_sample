'use strict';
const ObjectId = require('mongoose').Types.ObjectId;

class Stock_model {
    constructor() {
        this.symbol_schema = require('../schemas/symbol.schema');
    }
    get_symbols(){
       return this.symbol_schema.find();
    }
}

module.exports = new Stock_model();