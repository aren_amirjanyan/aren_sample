'use strict';
const ObjectId = require('mongoose').Types.ObjectId;

class Stock_model {
    constructor() {
        this.stock_schema = require('../schemas/stock.schema');
    }
    get_stockes(symbol){
       return this.stock_schema.find();
    }
}

module.exports = new Stock_model();