const   symbol_model = require('../models/symbol.model');
        symbol_service = require('../services/symbol.service');


class SymbolController {
    get_symbols(req, res, next) {
        return symbol_service.get_symbols()
            .then(data=>res.json(data))
            .catch(error=>next(error));
    }
}

module.exports = new SymbolController();