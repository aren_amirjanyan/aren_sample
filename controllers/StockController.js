const   stock_model = require('../models/stock.model');
        stock_service = require('../services/stock.service');


class StockController {
    get_stock(req, res, next) {
        return stock_service.get_stock(req.params.symbol)
            .then(data=>res.json(data))
            .catch(error=>next(error));
    }
}

module.exports = new StockController();